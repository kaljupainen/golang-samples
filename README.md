# Golang Samples

```
package main

import "fmt"

func main() {
	fmt.Printf("Hello world\n")
}
```

Ejemplo de bucles:

```
package main

import (
	"bytes"
	"fmt"
	"io"
)

func main() {
	/* Create a new buffer string separated by | character */
	buf := bytes.NewBufferString("one|two|three|four|")
	for {
		line, err := buf.ReadString('|')
		if err != nil && err == io.EOF {
			break
		}

		/* Remove the last character */
		fmt.Println(line[:len(line)-1], " ", err)
	}

	/* Another way to read arrays; use _ if don't want to use the index */
	elements := []string{"element1", "element2", "element3"}
	for _, element := range elements {
		fmt.Println(element)
	}
}
```

El uso de punteros:

```
package main

import (
	"fmt"
)

func main() {
	var pointerToCadena *string;	
	cadena := "hola ke ase";
	pointerToCadena = &cadena;
	fmt.Println("La cadena es ", cadena);
	fmt.Println("La dirección de la cadena es ", pointerToCadena);

	hiByValue(cadena);
	hiByReference(&cadena);
}

func hiByValue(cadena string) {
	fmt.Println(cadena);
}

func hiByReference(cadena *string) {
	fmt.Println(*cadena);
}
```

Condiciones if y switch:

```
package main

import "fmt"

func main() {
    grade := 60
	execute := true

    if grade >= 65 {
        fmt.Println("Passing grade")
	} else if (grade == 0) {
		fmt.Println("zero zapatero")
	} else {
		fmt.Println("Nop")
		if execute {
			fmt.Println("kaka")
		}
	}

	switch grade {
		case grade > 60:
			fmt.Println("OK");
		case grade < 60:
			fmt.Println("KO");
		default:
			fmt.Println("OTRO");
	}
}
```

Un servicio muy sencillo, utilizando el framework gin. Simplemente response a la url http://0.0.0.0:8080/ping.

Ejecuta estas instrucciones para iniciar
el proyecto:

```
go mod init service
```
```
go mod tidy
```
Y para ejecutar:

```
go run service.go
```

El código del servicio es:

```
package main

import "github.com/gin-gonic/gin"

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
```

